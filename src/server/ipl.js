//////////////////////////////////////////////////////////////////USING FOR LOOPS////////////////////////////////



function matchesPerYear(matches) {
    let UniqueSeasons = [];
    UniqueSeasons.push(matches[0].season);
    for (let i = 1; i < matches.length; i++) {
        if (matches[i].season != matches[i - 1].season) {
            UniqueSeasons.push(matches[i].season);
        }
    }

    let countMatchesPerYear = [];
    for (let i = 0; i < UniqueSeasons.length; i++) {
        let count = 0;
        for (let j = 0; j < matches.length; j++) {
            if (UniqueSeasons[i] === matches[j].season) {
                count++;
            }

        }
        countMatchesPerYear.push(count);
    }
    let matchesPerSeasonArray = [];

    for (let i = 0; i < UniqueSeasons.length; i++) {
        let obj = {};
        obj.season = UniqueSeasons[i];
        obj.matches = countMatchesPerYear[i];
        matchesPerSeasonArray.push(obj);
    }
    return matchesPerSeasonArray;
}
exports.matchesPerYear = matchesPerYear;





function winsPerTeamPerYear(matches) {

    let UniqueSeasons = [];
    UniqueSeasons.push(matches[0].season);
    for (let i = 1; i < matches.length; i++) {
        if (matches[i].season != matches[i - 1].season) {
            UniqueSeasons.push(matches[i].season);
        }
    }

    let UniqueTeams = [];
    let totalTeams = [];
    for (let i = 0; i < matches.length; i++) {
        totalTeams.push(matches[i].team1);
    }
    totalTeams.sort();
    UniqueTeams.push(totalTeams[0]);
    for (let i = 1; i < matches.length; i++) {
        if (totalTeams[i] != totalTeams[i - 1]) {
            UniqueTeams.push(totalTeams[i]);
        }
    }
    let obj = {};
    obj.teams = {};
    for (let i = 0; i < UniqueTeams.length; i++) {
        obj.teams[UniqueTeams[i]] = {};
        for (let j = 0; j < UniqueSeasons.length; j++) {
            obj.teams[UniqueTeams[i]][UniqueSeasons[j]] = {};
            // obj.teams[UniqueTeams[i]].year
            obj.teams[UniqueTeams[i]][UniqueSeasons[j]].wins;
        }
    }
    for (let i = 0; i < UniqueTeams.length; i++) {
        for (let j = 0; j < UniqueSeasons.length; j++) {
            let count = 0;
            for (let k = 0; k < matches.length; k++) {
                if ((UniqueTeams[i] === matches[k].winner) && (UniqueSeasons[j] === matches[k].season)) {
                    count++;
                }
            }
            obj.teams[UniqueTeams[i]][UniqueSeasons[j]].wins = count;
        }
    }
    // console.log(obj.teams[UniqueTeams[0]][UniqueSeasons[0]].wins);
    let resObj = [];
    for (let i = 0; i < UniqueTeams.length; i++) {
        for (let j = 0; j < UniqueSeasons.length; j++) {
            resObj.push({
                teams: UniqueTeams[i],
                season: UniqueSeasons[j],
                wins: obj.teams[UniqueTeams[i]][UniqueSeasons[j]].wins
            });
        }
    }

    return resObj;
}
exports.winsPerTeamPerYear = winsPerTeamPerYear;



function extraRunsPerTeam2016(deliveries) {
    let deliveries_2016 = [];
    for (let i = 0; i < deliveries.length; i++) {
        if (deliveries[i].match_id >= 577 && deliveries[i].match_id <= 636) {
            deliveries_2016.push(deliveries[i]);
        }
    }
    // console.log(deliveries_2016);
    let teams_2016 = [];
    for (let i = 0; i < deliveries_2016.length; i++) {
        teams_2016.push(deliveries_2016[i].bowling_team);
    }
    teams_2016.sort();
    let UniqueTeams_2016 = [];
    UniqueTeams_2016.push(teams_2016[0]);
    for (let i = 1; i < deliveries_2016.length; i++) {
        if (teams_2016[i] != teams_2016[i - 1]) {
            UniqueTeams_2016.push(teams_2016[i]);
        }
    }
    // console.log(UniqueTeams_2016);
    // console.log(deliveries_2016);
    let extraRunsConceded = [];
    for (let i = 0; i < UniqueTeams_2016.length; i++) {
        let extraRuns = 0;
        for (let j = 0; j < deliveries_2016.length; j++) {
            if (UniqueTeams_2016[i] === deliveries_2016[j].bowling_team) {
                extraRuns += Number(deliveries_2016[j].extra_runs);
            }
        }
        let temp = {};
        temp.team = UniqueTeams_2016[i];
        temp.extrasConceded = extraRuns;
        extraRunsConceded.push(temp);
    }
    return extraRunsConceded;
}
exports.extraRunsPerTeam2016 = extraRunsPerTeam2016;

function top10EconomicBowlers(deliveries) {
    let deliveries_2015 = [];
    for (let i = 0; i < deliveries.length; i++) {
        if (deliveries[i].match_id >= 518 && deliveries[i].match_id <= 576) {
            deliveries_2015.push(deliveries[i]);
        }
    }
    // console.log(deliveries_2015);
    let bowlers_list = [];
    for (let i = 0; i < deliveries_2015.length; i++) {
        bowlers_list.push(deliveries_2015[i].bowler);
    }
    // console.log(bowlers_list);
    bowlers_list.sort();
    let UniqueBowlersList = [];
    UniqueBowlersList.push(bowlers_list[0]);
    for (let i = 1; i < bowlers_list.length; i++) {
        if (bowlers_list[i] != bowlers_list[i - 1]) {
            UniqueBowlersList.push(bowlers_list[i]);
        }
    }
    // console.log(UniqueBowlersList);

    // console.log(typeof deliveries_2015[0].total_runs);
    let totalRunsConcededByBowlers = [];
    for (let i = 0; i < UniqueBowlersList.length; i++) {
        let runs = 0;
        for (let j = 0; j < deliveries_2015.length; j++) {
            if ((UniqueBowlersList[i] === deliveries_2015[j].bowler) && (((deliveries_2015[j].bye_runs == 0) && (deliveries_2015[j].legbye_runs == 0)))) {
                runs += Number(deliveries_2015[j].total_runs);
            }
        }
        totalRunsConcededByBowlers.push(runs);
    }
    // console.log(totalRunsConcededByBowlers);
    let oversBowledByBowler = [];
    for (let i = 0; i < UniqueBowlersList.length; i++) {
        let balls = 0;
        for (let j = 0; j < deliveries_2015.length; j++) {
            if ((UniqueBowlersList[i] === deliveries_2015[j].bowler) && ((deliveries_2015[j].wide_runs == 0) && (deliveries_2015[j].noball_runs == 0))) {
                balls += 1;
            }
        }
        oversBowledByBowler.push(Math.floor(balls / 6));
    }
    // console.log(oversBowledByBowler);
    let economyRate = [];
    for (let i = 0; i < UniqueBowlersList.length; i++) {
        let temp = totalRunsConcededByBowlers[i] / oversBowledByBowler[i];
        economyRate.push(temp);
    }
    // console.log(economyRate);
    let economicRateObjs = [];
    for (let i = 0; i < UniqueBowlersList.length; i++) {
        let temp = {};
        temp.bowler_name = UniqueBowlersList[i];
        temp.economicRate = economyRate[i];
        economicRateObjs.push(temp);
    }
    // console.log(economicRateObjs);
    economicRateObjs.sort(function (a, b) {
        return a.economicRate - b.economicRate;
    })
    // console.log(economicRateObjs);
    let top10EconomicBowlersObj = [];
    for (let i = 0; i < 10; i++) {
        top10EconomicBowlersObj.push(economicRateObjs[i]);
    }
    // console.log(top10EconomicBowlersObj);

    return top10EconomicBowlersObj;
    // economyRate.sort(function(a,b){
    //     return b-a;
    // });
    // console.log(economyRate);
}
exports.top10EconomicBowlers = top10EconomicBowlers;


function tossWinsAndMatchWins(matches) {
    let teams = [];
    for (let i = 0; i < matches.length; i++) {
        teams.push(matches[i].team1);
    }
    teams.sort();
    // console.log(teams);
    let UniqueTeams = [];
    UniqueTeams.push(teams[0]);
    for (let i = 1; i < teams.length; i++) {
        if (teams[i] != teams[i - 1]) {
            UniqueTeams.push(teams[i]);
        }
    }
    // console.log(UniqueTeams);

    let tossAndWinsObjs = [];
    for (let i = 0; i < UniqueTeams.length; i++) {
        let count = 0;
        for (let j = 0; j < matches.length; j++) {
            if (UniqueTeams[i] === matches[j].toss_winner && UniqueTeams[i] === matches[j].winner) {
                count++;
            }
        }
        let temp = {};
        temp.team = UniqueTeams[i];
        temp.tossAndWins = count;
        tossAndWinsObjs.push(temp);
    }
    return tossAndWinsObjs;
}
exports.tossWinsAndMatchWins = tossWinsAndMatchWins;

function mostPOTMWinsPerSeason(matches) {
    // console.log(matches[0]);
    let POTMwinners = [];
    for (let i = 0; i < matches.length; i++) {
        POTMwinners.push(matches[i].player_of_match);
    }
    // console.log(POTMwinners);
    POTMwinners.sort();


    let UniquePOTMwinners = [];
    UniquePOTMwinners.push(POTMwinners[0]);
    for (let i = 1; i < matches.length; i++) {
        if (POTMwinners[i] != POTMwinners[i - 1]) {
            UniquePOTMwinners.push(POTMwinners[i]);
        }
    }
    // console.log(UniquePOTMwinners);
    let POTMwinnersObj = [];
    for (let i = 2008; i <= 2017; i++) {
        for (let j = 0; j < UniquePOTMwinners.length; j++) {
            let obj = {};
            let count = 0;
            for (let k = 0; k < matches.length; k++) {
                if ((i == matches[k].season) && (UniquePOTMwinners[j] == matches[k].player_of_match)) {
                    count++;
                }
            }
            obj.season = i;
            obj.player = UniquePOTMwinners[j];
            obj.wins = count;
            POTMwinnersObj.push(obj);
        }
    }
    POTMwinnersObj.sort(function (a, b) {
        return b.wins - a.wins;
    })
    // console.log(POTMwinnersObj);
    POTMwinnersObj.sort(function (a, b) {
        return a.season - b.season;
    })
    // console.log(POTMwinnersObj);
    let mostPOTMwinnersPerYear = [];
    mostPOTMwinnersPerYear.push(POTMwinnersObj[0]);
    for (let i = 1; i < POTMwinnersObj.length; i++) {
        if (POTMwinnersObj[i].season != POTMwinnersObj[i - 1].season) {
            mostPOTMwinnersPerYear.push(POTMwinnersObj[i]);
        }
    }
    // console.log(mostPOTMwinnersPerYear);
    return mostPOTMwinnersPerYear;
}
exports.mostPOTMWinsPerSeason = mostPOTMWinsPerSeason;

function strikeRatePerPlayerPerSeason(deliveries) {
    let matchIdData = [{
            year: "2008",
            min_id: 60,
            max_id: 117
        },
        {
            year: "2009",
            min_id: 118,
            max_id: 174
        },
        {
            year: "2010",
            min_id: 175,
            max_id: 234
        },
        {
            year: "2011",
            min_id: 235,
            max_id: 307
        },
        {
            year: "2012",
            min_id: 308,
            max_id: 381
        },
        {
            year: "2013",
            min_id: 382,
            max_id: 457
        },
        {
            year: "2014",
            min_id: 458,
            max_id: 517
        },
        {
            year: "2015",
            min_id: 518,
            max_id: 576
        }, {
            year: "2016",
            min_id: 577,
            max_id: 636
        },
        {
            year: "2017",
            min_id: 1,
            max_id: 59
        }
    ];
    let dataByYears = [];
    for (let i = 0; i < matchIdData.length; i++) {
        let obj = {};
        obj.year = matchIdData[i].year;
        obj.data = [];
        for (let j = 0; j < deliveries.length; j++) {
            if (deliveries[j].match_id >= matchIdData[i].min_id && deliveries[j].match_id <= matchIdData[i].max_id) {
                obj.data.push(deliveries[j]);
            }
        }
        dataByYears.push(obj);
    }
    // console.log(dataByYears);
    let batsmanListByYear = [];
    for (let i = 0; i < dataByYears.length; i++) {
        let obj = {};
        obj.year = dataByYears[i].year;
        obj.batsman = [];
        for (let j = 0; j < dataByYears[i].data.length; j++) {
            obj.batsman.push(dataByYears[i].data[j].batsman);
            // console.log(dataByYears[i].data[j].batsman);
        }
        obj.batsman.sort();
        batsmanListByYear.push(obj);
    }

    // console.log(batsmanListByYear);
    let UniqueBatsmanPerYear = [];
    for (let i = 0; i < batsmanListByYear.length; i++) {
        let obj = {};
        obj.year = batsmanListByYear[i].year;
        obj.uniqueBatsmanList = [];
        obj.uniqueBatsmanList.push(batsmanListByYear[i].batsman[0]);
        for (let j = 1; j < batsmanListByYear[i].batsman.length; j++) {
            if (batsmanListByYear[i].batsman[j] != batsmanListByYear[i].batsman[j - 1]) {
                obj.uniqueBatsmanList.push(batsmanListByYear[i].batsman[j]);
            }
        }
        UniqueBatsmanPerYear.push(obj);
    }
    // console.log(UniqueBatsmanPerYear);
    // let ArrayObjs=[];
    // for(let i=0;i<UniqueBatsmanPerYear.length;i++){
    //     let obj={};
    //     obj.year=UniqueBatsmanPerYear[i].year;
    //     obj.batsmen=[];
    //     for(let j=0;j<UniqueBatsmanPerYear[i].uniqueBatsmanList.length;j++){
    //         let temp={};
    //         temp.batsman=UniqueBatsmanPerYear[i].uniqueBatsmanList[j];
    //         let ballsCount=0;
    //         let totalRuns=0;
    //         for(let k=0;k<dataByYears[i].length;k++){
    //             if(temp.batsman==dataByYears[i].batsman[k]){
    //                 console.log("hello");
    //             }
    //         }
    //         temp.ballsPlayed=ballsCount;
    //         obj.batsmen.push(temp);
    //     }
    //     ArrayObjs.push(obj);
    // }
    // console.log(ArrayObjs[0].batsmen);

    let tempArrayObjs = [];
    for (let i = 0; i < UniqueBatsmanPerYear.length; i++) {
        let obj = {};
        obj.year = UniqueBatsmanPerYear[i].year;
        obj.batsmen_stats = [];
        for (let j = 0; j < UniqueBatsmanPerYear[i].uniqueBatsmanList.length; j++) {
            let temp = {};
            temp.name = UniqueBatsmanPerYear[i].uniqueBatsmanList[j];
            // obj.batsmen_stats.push(temp);
            temp.runs = 0;
            temp.balls_faced = 0;
            for (let k = 0; k < deliveries.length; k++) {
                if (deliveries[k].match_id >= matchIdData[i].min_id && (deliveries[k].match_id <= matchIdData[i].max_id)) {
                    if (temp.name === deliveries[k].batsman) {
                        temp.runs += Number(deliveries[k].total_runs);
                        temp.balls_faced = temp.balls_faced + 1;
                    }
                }
            }
            temp.strikeRate = ((temp.runs / temp.balls_faced) * 100).toFixed(2);
            obj.batsmen_stats.push(temp);
        }
        tempArrayObjs.push(obj);
    }
    return tempArrayObjs;
}
exports.strikeRatePerPlayerPerSeason = strikeRatePerPlayerPerSeason;


function mostPlayerDismissals(deliveries) {
    let dismissalStats = [];
    for (let i = 0; i < deliveries.length; i++) {
        if (deliveries[i].player_dismissed != "") {
            let obj = {};
            obj.dismissed_player = deliveries[i].player_dismissed;
            if (deliveries[i].dismissal_kind === "caught" || deliveries[i].dismissal_kind === "run out") {
                obj.dismissed_by = deliveries[i].fielder;
            } else {
                if (deliveries[i].dismissal_kind === "bowled" || deliveries[i].dismissal_kind === "lbw" || deliveries[i].dismissal_kind === "caught and bowled") {
                    obj.dismissed_by = deliveries[i].bowler;
                }
            }
            dismissalStats.push(obj);
        }
    }
    // console.log(dismissalStats);
    // dismissalStats.sort();
    // console.log(dismissalStats);
    let dismissedPlayers = [];
    let dismissed_by = [];
    for (let i = 0; i < dismissalStats.length; i++) {
        dismissedPlayers.push(dismissalStats[i].dismissed_player);
        dismissed_by.push(dismissalStats[i].dismissed_by);
    }
    dismissedPlayers.sort();
    // dismissed_by.sort();
    // console.log(dismissedPlayers);
    // console.log(dismissed_by);
    let UniquedismissedPlayers = [];
    let UniquePlayersDismissedBy = [];
    UniquedismissedPlayers.push(dismissedPlayers[0])
    UniquePlayersDismissedBy.push(dismissed_by[0]);
    for (let i = 1; i < dismissalStats.length; i++) {
        if (dismissedPlayers[i] != dismissedPlayers[i - 1]) {
            UniquedismissedPlayers.push(dismissedPlayers[i]);
        }
        if (dismissed_by[i] != dismissed_by[i - 1]) {
            UniquePlayersDismissedBy.push(dismissed_by[i]);
        }
    }
    // console.log(UniquedismissedPlayers.length);
    // console.log(UniquePlayersDismissedBy.length);
    // let ArrayObjs=[];
    // for(let i=0;i<UniquedismissedPlayers.length;i++){
    //     for(let j=0;j<UniquePlayersDismissedBy.length;j++){
    //         let count=0;
    //         let obj={};
    //         for(let k=0;k<dismissalStats.length;k++){
    //             if(UniquedismissedPlayers[i]==dismissalStats.dismissed_player&&UniquePlayersDismissedBy[i]==dismissalStats.dismissed_by){
    //                 count++;
    //             }
    //         }
    //         obj.dismissed=UniquedismissedPlayers[i];
    //         obj.dismissed_by=UniquePlayersDismissedBy[i];
    //         obj.times=count;
    //         ArrayObjs.push(obj);
    //     }
    // }
    // console.log(ArrayObjs);
    let ArrayObjs=[];
    for(let i=0;i<UniquedismissedPlayers.length;i++){
        let obj={};
        obj.player_dismissed=UniquedismissedPlayers[i];
        let count=0;
        for(let j=0;j<dismissalStats.length;j++){
            if(UniquedismissedPlayers[i]===dismissalStats[j].dismissed_player){
                count++;
            }
        }
        obj.times=count;
        ArrayObjs.push(obj);
    }
    return ArrayObjs;
}
exports.mostPlayerDismissals = mostPlayerDismissals;


function bestEconomyInSuperOvers(deliveries){
let superOverStats=[];
for(let i=0;i<deliveries.length;i++){
    if(deliveries[i].is_super_over==1){
        superOverStats.push(deliveries[i]);
    }
}
// console.log(superOverStats);
// console.log(superOverStats.length);
let bowlersList=[];
for(let i=0;i<superOverStats.length;i++){
    bowlersList.push(superOverStats[i].bowler);
}
// console.log(bowlersList);
bowlersList.sort();
let UniqueBowlersList=[];
UniqueBowlersList.push(bowlersList[0]);
for(let i=1;i<bowlersList.length;i++){
if(bowlersList[i]!=bowlersList[i-1]){
    UniqueBowlersList.push(bowlersList[i]);
}
}
// console.log(UniqueBowlersList);
let bowlerSuperOverStats=[];
for(let i=0;i<UniqueBowlersList.length;i++){
    let runs=0;
    let balls=0;
    let obj={};
    obj.bowler=UniqueBowlersList[i];
    for(let j=0;j<superOverStats.length;j++){
        if(UniqueBowlersList[i]===superOverStats[j].bowler&&superOverStats[j].bye_runs==0&&superOverStats[j].legbye_runs==0){
            runs+=Number(superOverStats[j].total_runs);
        }
        if(UniqueBowlersList[i]===superOverStats[j].bowler&&superOverStats[j].wide_runs==0&&superOverStats[j].noball_runs==0){
            balls++;
        }
    }
    obj.runsConceded=runs;
    obj.ballsDelivered=balls;
    obj.economy=((runs/balls).toFixed(2));
    bowlerSuperOverStats.push(obj);
}
// console.log(bowlerSuperOverStats);
// console.log(superOverStats[0]);
let bestEconomyRate=Infinity;
let index=0;
for(let i=0;i<bowlerSuperOverStats.length;i++){
    if(Number(bowlerSuperOverStats[i].economy)<bestEconomyRate){
        bestEconomyRate=Number(bowlerSuperOverStats[i].economy);
        index=i;
    }
}
// console.log(bowlerSuperOverStats[index]);
return bowlerSuperOverStats[index];
}
exports.bestEconomyInSuperOvers=bestEconomyInSuperOvers;