const csv = require("csvtojson");

//////////////////////////////////////////CSV Paths//////////////////////////////////////////////////////////////
const matchPath = ("../data/matches.csv");
const deliveriesPath = ("../data/deliveries.csv");
//---------------------------------------------------------------------------------------------------------------

/////////////////////////////////////////Function modules//////////////////////////////////////////////////////////
let mainObj = require("./ipl");
// console.log(mainObj);
//----------------------------------------------------------------------------------------------------------------

/////////////////////////////////////////Matches played per year for every year////////////////////////////////////
let matchesPerYear = mainObj.matchesPerYear;
let resMatchesPerYear;
//-----------------------------------------------------------------------------------------------------------------

////////////////////////////////////////Number of matches won per team per year///////////////////////////////////
let winsPerTeamPerYear = mainObj.winsPerTeamPerYear;
// console.log(winsPerTeamPerYear);
let resWinsPerTeamPerYear;
//-----------------------------------------------------------------------------------------------------------------

///////////////////////////////////////Extra runs conceded per team per year//////////////////////////////////////

let extraRunsPerTeam2016 = mainObj.extraRunsPerTeam2016;
// console.log(extraRunsPerTeam2016);
//----------------------------------------------------------------------------------------------------------------

//////////////////////////////////////Top 10 economic bowlers 2015////////////////////////////////////////////////

let top10EconomicBowlers = mainObj.top10EconomicBowlers;
// console.log(top10EconomicBowlers);

//----------------------------------------------------------------------------------------------------------------

////////////////////////////////////Number of times each team won the toss and then won the match/////////////////

let tossWinsAndMatchWins = mainObj.tossWinsAndMatchWins;
// console.log(tossWinsAndMatchWins);

//----------------------------------------------------------------------------------------------------------------

//////////////////////////////////Most POTM wins by a player each season//////////////////////////////////////////

let mostPOTMWinsPerSeason=mainObj.mostPOTMWinsPerSeason;
// console.log(mostPOTMWinsPerSeason);

//----------------------------------------------------------------------------------------------------------------

    //////////////////////////////////Strike rate of a batsman for each season////////////////////////////////////////
    let strikeRatePerPlayerPerSeason=mainObj.strikeRatePerPlayerPerSeason;
    // console.log(strikeRatePerPlayerPerSeason);
    //----------------------------------------------------------------------------------------------------------------


    //////////////////////////////////Most dismissals of a player by another player///////////////////////////////////

    let mostPlayerDismissals=mainObj.mostPlayerDismissals;
    // console.log(mostPlayerDismissals);
    //----------------------------------------------------------------------------------------------------------------


    //////////////////////////////////Best economy in super overs//////////////////////////////////////////////////////
    let bestEconomyInSuperOvers=mainObj.bestEconomyInSuperOvers;
    // console.log(bestEconomyInSuperOvers);

    //-----------------------------------------------------------------------------------------------------------------

function main() {

    /////////////////////////////////////////Matches played per year for every year////////////////////////////////////

    csv().fromFile(matchPath)
    .then(matches =>{
        resMatchesPerYear = matchesPerYear(matches);
        console.log("Matches played per year in IPL");
        console.log("***************************************************************************");
        console.log(resMatchesPerYear);
    })


    ////////////////////////////////////////Number of matches won per team per year///////////////////////////////////

    csv().fromFile(matchPath)
    .then(matches =>{
        resWinsPerTeamPerYear=winsPerTeamPerYear(matches);
        console.log("Number of matches won per team per year");
        console.log("***************************************************************************");
        console.log(resWinsPerTeamPerYear);
    })

    //----------------------------------------------------------------------------------------------------------------


    //////////////////////////////////////Extra runs conceded per team in 2016////////////////////////////////////////

    csv().fromFile(deliveriesPath)
    .then((deliveries) =>{
        resExtraRunsPerTeam2016=extraRunsPerTeam2016(deliveries);
        console.log("Extra runs conceded per team in 2016");
        console.log("***************************************************************************");
        console.log(resExtraRunsPerTeam2016);
    })


    //-----------------------------------------------------------------------------------------------------------------

    /////////////////////////////////////Top 10 economic bowlers in 2015///////////////////////////////////////////////

    csv().fromFile(deliveriesPath)
    .then((deliveries) =>{
        resTop10EconomicalBowlers=top10EconomicBowlers(deliveries);
        console.log("Top 10 economic bowlers in 2015");
        console.log("***************************************************************************");
        console.log(resTop10EconomicalBowlers);
    })

    //-----------------------------------------------------------------------------------------------------------------






    ////////////////////////////////////Number of times each team won the toss and then won the match///////////////////

    csv().fromFile(matchPath)
    .then((matches) =>{
        resTossWinsAndMatchWins=tossWinsAndMatchWins(matches);
        console.log("Number of times a team won the toss and then won the match");
        console.log("***************************************************************************");
        console.log(resTossWinsAndMatchWins);
    })

    //------------------------------------------------------------------------------------------------------------------


    ///////////////////////////////////Most POTM wins by each player in a season///////////////////////////////////////

    csv().fromFile(matchPath)
        .then((matches) => {
            resMostPOTMWinsPerSeason=mostPOTMWinsPerSeason(matches);
            console.log("Most POTM wins by a player in a season");
            console.log("***************************************************************************");
            console.log(resMostPOTMWinsPerSeason);
        })

    //-----------------------------------------------------------------------------------------------------------------

    //////////////////////////////////Strike rate of a batsman for each season////////////////////////////////////////

    csv().fromFile(deliveriesPath)
        .then((deliveries) => {
            resStrikeRatePerPlayerPerSeason=strikeRatePerPlayerPerSeason(deliveries);
            console.log("Strike rate of a batsman for each season of IPL");
            console.log("***************************************************************************");
            console.log(resStrikeRatePerPlayerPerSeason);
        })
    //----------------------------------------------------------------------------------------------------------------


    ////////////////////////////////Highest dismissals of a player by another player//////////////////////////////////

    csv().fromFile(deliveriesPath)
        .then((deliveries) => {
            resMostPlayerDismissals=mostPlayerDismissals(deliveries);
            console.log("Highest dismissals of a player by another player");
            console.log("***************************************************************************");
            console.log(resMostPlayerDismissals);
        })

    //----------------------------------------------------------------------------------------------------------------


    ///////////////////////////////Best economy in super overs////////////////////////////////////////////////////////
    csv().fromFile(deliveriesPath)
    .then((deliveries) => {
        resBestEconomy=bestEconomyInSuperOvers(deliveries);
        console.log("Best economy in super overs");
        console.log("***************************************************************************");
        console.log(resBestEconomy);
    })

    //----------------------------------------------------------------------------------------------------------------


}
main();